var gulp = require('gulp');

// Load the packages
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglifyjs');
var less = require('gulp-less');
var rename= require('gulp-rename');
var livereload_server = require('gulp-server-livereload');

// var livereload = require('gulp-livereload');


gulp.task('css', function () { 
// Tells Gulp where to find my file 
 return gulp.src('src/assets/css/styles.less') 

// Compile LESS 
 .pipe(less())


// Will autoprefix the css                      
.pipe(autoprefixer()) 

// Rename the file with suffix
.pipe(rename({
       suffix: '.min'
   }))

// The final destination             
.pipe(gulp.dest('dist/assets/css/'))

// Needs the live reload plugin
// .pipe(livereload());

});

gulp.task('js', function () {
	 // The js files location
 return gulp.src('src/assets/js/global.js') 

                   
 // Will minify the JS
 .pipe(uglify())      

 // Rename the file with suffix
.pipe(rename({
       suffix: '.min'
   }))

 // The final destination
 .pipe(gulp.dest('dist/assets/js')); 

});


// Prepare a live reload server : https://www.npmjs.com/package/gulp-server-livereload 
gulp.task('webserver', function() {
  gulp.src('')
    .pipe(livereload_server({
    defaultFile: 'index.html',	
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

gulp.task('watch', function () {
	// Needs the live reload plugin
	// livereload.listen();

	gulp.watch('src/assets/css/**/*.less', ['css']);
	gulp.watch('src/assets/js/**/*.js', ['js']);
});

gulp.task('default', ['watch', 'webserver']);