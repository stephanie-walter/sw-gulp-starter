var gulp = require('gulp');

// Load the packages
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var less = require('gulp-less');
var rename= require('gulp-rename');
// var uglify = require('gulp-uglifyjs'); 
var browserSync = require('browser-sync');
var reload = browserSync.reload;


// Env vars
var csspath = './styles/';
var proxypath = 'localhost/sites/inpixelitrust/blog';



// Gulp CSS task: LESS, autoprefix, rename
gulp.task('css', function () { 
  // Tells Gulp where to find my file 
  return gulp.src(csspath + 'style.less') 

    // Compile LESS 
    .pipe(less())
  
    // Will autoprefix the css                    
    .pipe(autoprefixer()) 
  
    // Rename the file with suffix
    .pipe(rename({
      suffix: '.min'
    }))
  
    // The final destination             
    .pipe(gulp.dest(csspath))

    // For browser sync
    .pipe(reload({ stream:true }));
});



/*
gulp.task('js', function () {
	 // The js files location
 return gulp.src('src/assets/js/global.js') 

  // Will minify the JS
  .pipe(uglify())      

    // Rename the file with suffix
    .pipe(rename({
       suffix: '.min'
   }))

 // The final destination
 .pipe(gulp.dest('dist/assets/js')); 

});

*/


gulp.task('work', function () {
  gulp.watch(csspath + '*.less', ['css']);
  // gulp.watch('src/assets/js/**/*.js', ['js']);
  
  // Start the browsersync server
  browserSync({
    proxy: proxypath
  });
  
  // Watching PHP and CSS files for change on WP
  gulp.watch(['./*.php', csspath + '*.css'], reload);  
  
});